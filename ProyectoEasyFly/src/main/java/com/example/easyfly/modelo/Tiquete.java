package com.example.easyfly.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = Tiquete.TABLE_NAME)
public class Tiquete {

    public static final String TABLE_NAME = "tiquete";

    //Llave primaria
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTiquete;
    private int idUsuario;
    private int idRuta;
    private int precioTotal;
    
    //Constructores

    public Tiquete() {
    }

    public Tiquete(int idTiquete, int idUsuario, int idRuta, int precioTotal) {
        this.idTiquete = idTiquete;
        this.idUsuario = idUsuario;
        this.idRuta = idRuta;
        this.precioTotal = precioTotal;
    }

    public int getIdTiquete() {
        return idTiquete;
    }

    public void setIdTiquete(int idTiquete) {
        this.idTiquete = idTiquete;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(int idRuta) {
        this.idRuta = idRuta;
    }

    public int getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(int precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public String toString() {
        return "Tiquete{" + "idTiquete=" + idTiquete + ", idUsuario=" + idUsuario + ", idRuta=" + idRuta + ", precioTotal=" + precioTotal + '}';
    }
    
}
    
    