package com.example.easyfly.modelo;

import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = Ruta.TABLE_NAME)
public class Ruta {

    public static final String TABLE_NAME = "ruta";

    //Llave primaria
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idRuta;
    private String matriculaAeronave;
    private String numeroVuelo;
    private String ciudadOrigen;
    private String ciudadDestino;
    private int pasajeros;
    private Time horaLlegada;
    private Time horaSalida;
    private Double equipajeAdicional;
    private Double precio;

    //Constructores

    public Ruta() {
    }

    public Ruta(int idRuta, String matriculaAeronave, String numeroVuelo, String ciudadOrigen, String ciudadDestimo, int pasajeros, Time horaLlegada, Time horaSalida, Double equipajeAdicional, Double precio) {
        this.idRuta = idRuta;
        this.matriculaAeronave = matriculaAeronave;
        this.numeroVuelo = numeroVuelo;
        this.ciudadOrigen = ciudadOrigen;
        this.ciudadDestino = ciudadDestimo;
        this.pasajeros = pasajeros;
        this.horaLlegada = horaLlegada;
        this.horaSalida = horaSalida;
        this.equipajeAdicional = equipajeAdicional;
        this.precio = precio;
    }

    public int getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(int idRuta) {
        this.idRuta = idRuta;
    }

    public String getMatriculaAeronave() {
        return matriculaAeronave;
    }

    public void setMatriculaAeronave(String matriculaAeronave) {
        this.matriculaAeronave = matriculaAeronave;
    }

    public String getNumeroVuelo() {
        return numeroVuelo;
    }

    public void setNumeroVuelo(String numeroVuelo) {
        this.numeroVuelo = numeroVuelo;
    }

    public String getCiudadOrigen() {
        return ciudadOrigen;
    }

    public void setCiudadOrigen(String ciudadOrigen) {
        this.ciudadOrigen = ciudadOrigen;
    }

    public String getCiudadDestino() {
        return ciudadDestino;
    }

    public void setCiudadDestino(String ciudadDestimo) {
        this.ciudadDestino = ciudadDestimo;
    }

    public int getPasajeros() {
        return pasajeros;
    }

    public void setPasajeros(int pasajeros) {
        this.pasajeros = pasajeros;
    }

    public Time getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(Time horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public Time getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Time horaSalida) {
        this.horaSalida = horaSalida;
    }

    public Double getEquipajeAdicional() {
        return equipajeAdicional;
    }

    public void setEquipajeAdicional(Double equipajeAdicional) {
        this.equipajeAdicional = equipajeAdicional;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Ruta{" + "idRuta=" + idRuta + ", matriculaAeronave=" + matriculaAeronave + ", numeroVuelo=" + numeroVuelo + ", ciudadOrigen=" + ciudadOrigen + ", ciudadDestino=" + ciudadDestino + ", pasajeros=" + pasajeros + ", horaLlegada=" + horaLlegada + ", horaSalida=" + horaSalida + ", equipajeAdicional=" + equipajeAdicional + ", precio=" + precio + '}';
    }
    
    
}
