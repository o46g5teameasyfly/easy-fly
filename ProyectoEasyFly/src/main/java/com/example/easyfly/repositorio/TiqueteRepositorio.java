
package com.example.easyfly.repositorio;

import com.example.easyfly.modelo.Tiquete;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TiqueteRepositorio extends JpaRepository <Tiquete, Integer>{
    
}
