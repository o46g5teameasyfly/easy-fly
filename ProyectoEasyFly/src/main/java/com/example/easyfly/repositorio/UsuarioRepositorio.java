
package com.example.easyfly.repositorio;

import com.example.easyfly.modelo.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UsuarioRepositorio extends JpaRepository <Usuario, Integer>{
    
}
