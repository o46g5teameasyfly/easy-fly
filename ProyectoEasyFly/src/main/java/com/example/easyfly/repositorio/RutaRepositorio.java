
package com.example.easyfly.repositorio;

import com.example.easyfly.modelo.Ruta;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RutaRepositorio extends JpaRepository <Ruta, Integer>{
    
}
