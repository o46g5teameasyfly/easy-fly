
package com.example.easyfly.controlador;

import com.example.easyfly.modelo.Ruta;
import com.example.easyfly.servicio.RutaServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/rutas")
public class RutaController {
    
    @Autowired
    private RutaServicio rutaServicio;

    // Listar los registros de la Tabla
    @GetMapping("/list")
    public List<Ruta> consultarTodo() {
        return (rutaServicio.getRutas());
    }

    // Buscar por Id
    @GetMapping("/list/{idRuta}")
    public Ruta buscarPorIdRuta(@PathVariable Integer idRuta) {
        return rutaServicio.getRuta(idRuta);
    }

    // Crear o insertar un registro
    @PostMapping("/")
    public ResponseEntity<Ruta> agregar(@RequestBody Ruta ruta) {
        Ruta obj = rutaServicio.grabarRuta(ruta);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    // Actualización de un registro
    @PutMapping("/")
    public ResponseEntity<Ruta> editar(@RequestBody Ruta ruta) {
        Ruta obj = rutaServicio.getRuta(ruta.getIdRuta());
        if (obj != null) {
            obj.setMatriculaAeronave(ruta.getMatriculaAeronave());
            obj.setNumeroVuelo(ruta.getNumeroVuelo());
            obj.setCiudadOrigen(ruta.getCiudadOrigen());           
            obj.setCiudadDestino(ruta.getCiudadDestino());
            obj.setPasajeros(ruta.getPasajeros());
            obj.setHoraLlegada(ruta.getHoraLlegada());
            obj.setHoraSalida(ruta.getHoraSalida());           
            obj.setEquipajeAdicional(ruta.getEquipajeAdicional());
            obj.setPrecio(ruta.getPrecio());
            
            
            
            rutaServicio.grabarRuta(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    // Elimina un registro 
    @DeleteMapping("/{idRuta}")
    public ResponseEntity<Ruta> eliminar(@PathVariable Integer idRuta) {
    	Ruta obj = rutaServicio.getRuta(idRuta);
    	if(obj != null) {
        	rutaServicio.delete(idRuta);
    	}else {
    		return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    

}