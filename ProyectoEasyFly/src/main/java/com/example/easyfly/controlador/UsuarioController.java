
package com.example.easyfly.controlador;

import com.example.easyfly.modelo.Usuario;
import com.example.easyfly.servicio.UsuarioServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/usuarios")
public class UsuarioController {
    
    @Autowired
    private UsuarioServicio usuarioServicio;

    // Listar los registros de la Tabla
    @GetMapping("/list")
    public List<Usuario> consultarTodo() {
        return (usuarioServicio.getUsuarios());
    }

    // Buscar por Id
    @GetMapping("/list/{idUsuario}")
    public Usuario buscarPorIdUsuario(@PathVariable Integer idUsuario) {
        return usuarioServicio.getUsuario(idUsuario);
    }

    // Crear o insertar un registro
    @PostMapping("/")
    public ResponseEntity<Usuario> agregar(@RequestBody Usuario usuario) {
        Usuario obj = usuarioServicio.grabarUsuario(usuario);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    // Actualización de un registro
    @PutMapping("/")
    public ResponseEntity<Usuario> editar(@RequestBody Usuario usuario) {
        Usuario obj = usuarioServicio.getUsuario(usuario.getIdUsuario());
        if (obj != null) {
            obj.setTipoUsuario(usuario.getTipoUsuario());
            obj.setNumeroDocumento(usuario.getNumeroDocumento());
            obj.setEmail(usuario.getEmail());           
            obj.setNombre(usuario.getNombre());
            obj.setPasword(usuario.getPasword());
            usuarioServicio.grabarUsuario(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    // Elimina un registro 
    @DeleteMapping("/{idUsuario}")
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer idUsuario) {
    	Usuario obj = usuarioServicio.getUsuario(idUsuario);
    	if(obj != null) {
        	usuarioServicio.delete(idUsuario);
    	}else {
    		return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    

}