
package com.example.easyfly.controlador;

import com.example.easyfly.modelo.Tiquete;
import com.example.easyfly.servicio.TiqueteServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/tiquetes")
public class TiqueteController {
    
    @Autowired
    private TiqueteServicio tiqueteServicio;

    // Listar los registros de la Tabla
    @GetMapping("/list")
    public List<Tiquete> consultarTodo() {
        return (tiqueteServicio.getTiquetes());
    }

    // Buscar por Id
    @GetMapping("/list/{idTiquete}")
    public Tiquete buscarPorIdTiquete(@PathVariable Integer idTiquete) {
        return tiqueteServicio.getTiquete(idTiquete);
    }

    // Crear o insertar un registro
    @PostMapping("/")
    public ResponseEntity<Tiquete> agregar(@RequestBody Tiquete tiquete) {
        Tiquete obj = tiqueteServicio.grabarTiquete(tiquete);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    // Actualización de un registro
    @PutMapping("/")
    public ResponseEntity<Tiquete> editar(@RequestBody Tiquete tiquete) {
        Tiquete obj = tiqueteServicio.getTiquete(tiquete.getIdTiquete());
        if (obj != null) {
            obj.setIdUsuario(tiquete.getIdUsuario());
            obj.setIdRuta(tiquete.getIdRuta());
            obj.setPrecioTotal(tiquete.getPrecioTotal());           
            tiqueteServicio.grabarTiquete(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    // Elimina un registro 
    @DeleteMapping("/{idTiquete}")
    public ResponseEntity<Tiquete> eliminar(@PathVariable Integer idTiquete) {
    	Tiquete obj = tiqueteServicio.getTiquete(idTiquete);
    	if(obj != null) {
        	tiqueteServicio.delete(idTiquete);
    	}else {
    		return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
		return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    

}