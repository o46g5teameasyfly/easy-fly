package com.example.easyfly.servicio;

import com.example.easyfly.modelo.Usuario;
import com.example.easyfly.repositorio.UsuarioRepositorio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UsuarioServicio implements IUsuarioServicio {

    @Autowired
    private UsuarioRepositorio clienteRepo;

    //Listar los registros de la tabla (Read)
    @Override
    public List<Usuario> getUsuarios() {
        return clienteRepo.findAll();
    }

    //Buscar el Cliente por el id
    @Override
    public Usuario getUsuario(Integer idUsuario) {
        return clienteRepo.findById(idUsuario).orElse(null);
    }

    //Crear un registro en la tabla (insert into..) Create y Update
    @Override
    public Usuario grabarUsuario(Usuario usuario) {
        return clienteRepo.save(usuario);
    }

    //Elimina un registro Delete
    @Override
    public void delete(Integer id) {
        // TODO Auto-generated method stub
        clienteRepo.deleteById(id);
    }

}
