
package com.example.easyfly.servicio;

import com.example.easyfly.modelo.Tiquete;
import java.util.List;

public interface ITiqueteServicio {
    
    public List<Tiquete> getTiquetes();
    
    public Tiquete getTiquete(Integer idTiquete);
    
    public Tiquete grabarTiquete(Tiquete tiquete);
    
    public void delete(Integer idTiquete);

}
