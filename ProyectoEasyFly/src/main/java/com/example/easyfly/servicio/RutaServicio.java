package com.example.easyfly.servicio;

import com.example.easyfly.modelo.Ruta;
import com.example.easyfly.repositorio.RutaRepositorio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RutaServicio implements IRutaServicio {

    @Autowired
    private RutaRepositorio rutaRepo;

    //Listar los registros de la tabla (Read)
    @Override
    public List<Ruta> getRutas() {
        return rutaRepo.findAll();
    }

    //Buscar el Cliente por el id
    @Override
    public Ruta getRuta(Integer idRuta) {
        return rutaRepo.findById(idRuta).orElse(null);
    }

    //Crear un registro en la tabla (insert into..) Create y Update
    @Override
    public Ruta grabarRuta(Ruta ruta) {
        return rutaRepo.save(ruta);
    }

    //Elimina un registro Delete
    @Override
    public void delete(Integer id) {
        // TODO Auto-generated method stub
        rutaRepo.deleteById(id);
    }

}
