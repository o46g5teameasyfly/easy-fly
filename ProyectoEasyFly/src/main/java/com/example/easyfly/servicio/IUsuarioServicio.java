
package com.example.easyfly.servicio;

import com.example.easyfly.modelo.Usuario;
import java.util.List;

public interface IUsuarioServicio {
    
    public List<Usuario> getUsuarios();
    
    public Usuario getUsuario(Integer idUsuario);
    
    public Usuario grabarUsuario(Usuario usuario);
    
    public void delete(Integer idUsuario);

}
