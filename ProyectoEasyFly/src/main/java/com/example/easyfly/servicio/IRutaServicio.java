
package com.example.easyfly.servicio;

import com.example.easyfly.modelo.Ruta;
import java.util.List;

public interface IRutaServicio {
    
    public List<Ruta> getRutas();
    
    public Ruta getRuta(Integer idRuta);
    
    public Ruta grabarRuta(Ruta ruta);
    
    public void delete(Integer idRuta);

}
