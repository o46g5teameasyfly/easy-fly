package com.example.easyfly.servicio;

import com.example.easyfly.modelo.Tiquete;
import com.example.easyfly.repositorio.TiqueteRepositorio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TiqueteServicio implements ITiqueteServicio {

    @Autowired
    private TiqueteRepositorio tiqueteRepo;

    //Listar los registros de la tabla (Read)
    @Override
    public List<Tiquete> getTiquetes() {
        return tiqueteRepo.findAll();
    }

    //Buscar el Cliente por el id
    @Override
    public Tiquete getTiquete(Integer idTiquete) {
        return tiqueteRepo.findById(idTiquete).orElse(null);
    }

    //Crear un registro en la tabla (insert into..) Create y Update
    @Override
    public Tiquete grabarTiquete(Tiquete tiquete) {
        return tiqueteRepo.save(tiquete);
    }

    //Elimina un registro Delete
    @Override
    public void delete(Integer id) {
        // TODO Auto-generated method stub
        tiqueteRepo.deleteById(id);
    }

}
