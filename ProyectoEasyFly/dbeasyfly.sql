create database bdEasyFly;

use bdEasyFly;

create table usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT,
	tipoUsuario VARCHAR(45) NOT NULL,
	numeroDocumento VARCHAR(45) NOT NULL,
	email VARCHAR(45) NOT NULL,
	nombre VARCHAR(45) NOT NULL,
	pasword VARCHAR(45) NOT NULL,
	PRIMARY KEY (idUsuario)
    );


CREATE TABLE ruta (
  idRuta INT NOT NULL AUTO_INCREMENT,
  matriculaAeronave VARCHAR(45) NOT NULL,
  numeroVuelo VARCHAR(45) NOT NULL,
  ciudadOrigen VARCHAR(45) NOT NULL,
  ciudadDestino VARCHAR(45) NOT NULL,
  pasajeros INT NOT NULL,
  horaLlegada TIME NOT NULL,
  horaSalida TIME NOT NULL,
  equipajeAdicional DOUBLE NOT NULL,
  precio DOUBLE NOT NULL,
  PRIMARY KEY (idRuta)
  );
  
select * from usuario;
insert into usuario (tipoUsuario, numeroDocumento, email,
 nombre, pasword)
 values
	('administrador', 1143149735, 'otabordaparejo@gmail.com', 'omar taborda', 1234),
    ('administrador', 12545236, 'nohelia770@gmail.com', 'nohelia', 4321),
    ('administrador', 14587628, 'cesaar42sa@gmail.com', 'cesar', 8523),
    ('administrador', 125698743, 'natalia.p-20@outlook.com', 'natalia', 7896),
    ('administrador', 11256358, 'gerson@gmail.com', 'gerson', 4563);


CREATE TABLE tiquete (
  idTiquete INT NOT NULL AUTO_INCREMENT,
  idUsuario INT NOT NULL,
  idRuta INT NOT NULL,
  precioTotal INT NOT NULL,
  PRIMARY KEY (idTiquete),
  foreign key (idUsuario) references usuario (idUsuario),
  foreign key (idruta) references ruta (idRuta)
  );
  
  select * from usuario;
  
  insert into ruta (matriculaAeronave, numeroVuelo, ciudadOrigen, ciudadDestino, pasajeros, horaLlegada, horaSalida, equipajeAdicional,precio)
    value
		('HK2365', 'V11', 'Bogota', 'Bucaramanga', 120, 0600, 0700,30,100000),
        ('HK2365', 'V12', 'Bucaramanga', 'Bogota', 120, 0700, 0800, 30,80000),
        ('HK2366', 'V21', 'Bogota', 'Barranquilla', 120, 0700, 0800, 30,75000),
        ('HK2366', 'V12', 'Barranquilla', 'Bogota', 120, 0800, 0900, 30,120000),
        ('HK2367', 'V31', 'Bogota', 'Medellin', 120, 0800, 0900, 30,130000),
        ('HK2367', 'V32', 'Medellin', 'Bogota', 120, 0900, 1000, 30,90000),
        ('HK2368', 'V41', 'Bogota', 'Cali', 120, 0900, 1000, 30,152000),
        ('HK2368', 'V42', 'Cali', 'Bogota', 120, 1000, 1100, 30,85000);